## Introduction to Scala
![Scala](../SCALA.jpg)



### About us



#### Stathis Stergou
* Started professionally as a web programmer in PHP in 2002
* Teacher and hobbyist programmer 2005 - 2015
* Experimented with many programming languages (Scheme, Clojure, ML, Python, Haskell, Javascript, Java, PHP,  Scala and others)
* Scala programmer 2015 - now



#### Jack Tol
* Full stack developer (with a preference for back-end work)
* Scala and functional programming enthusiast since 2014
* Using Scala at a client since 2016


### What is Scala
Scala is a hybrid programming language whose purpose is to unify the object oriented with the functional programming paradigm.



### Personal Views about Scala
* Very powerful and concise language
* Strong theoretical background
* Multiparadigm programming language
* Unique combination of features
* Many different ways of doing things
* Principles arise as the language and the community evolves
* Becomes simpler as the time passes (Dotty)



### Successes and Failures
* Twitter
* Ing and other FinTechs
* Foursquare
* Tumblr
* The Guardian
* Airbnb
* LinkedIn?
* Yammer
* Heavily used in the "big data"  world (Spark)



### Theoretical Foundations
![Dotty Terms](../from-dot-to-dotty-12-638.jpg)



### Theoretical Foundations
* Type soundness property was shown with a mechanized, (i.e. machine-checked) proof.

* If a term t has type T, and the evaluation of t terminates, then the result of the evaluation will be a value v of type T.



### Scalable language
Designed to scale together with the needs of its users.
```scala
implicit def materializeCoproduct[T, K <: HList, 
  V <: Coproduct, R <: Coproduct]
	(implicit
	  lab: DefaultSymbolicLabelling.Aux[T, K],
	  gen: Generic.Aux[T, V],
	  zip: coproduct.ZipWithKeys.Aux[K, V, R],
	  ev: R <:< V
	): Aux[T, R] =
	new LabelledGeneric[T] {
	  type Repr = R
	  def to(t: T): Repr = zip(gen.to(t))
	  def from(r: Repr): T = gen.from(r)
	}
}
```
[shapeless@github](https://github.com/milessabin/shapeless/blob/master/core/src/main/scala/shapeless/generic.scala#L232)



### Scalable language
People still think it's a better java
```scala
 val buf = new ArrayBuffer[T]
 val totalParts = this.partitions.length
 var partsScanned = 0
 while (buf.size < num && partsScanned < totalParts) {
   var numPartsToTry = 1L
   if (partsScanned > 0) {
     if (buf.isEmpty) {
       numPartsToTry = partsScanned * scaleUpFactor
     } else {
       numPartsToTry =
         Math.max((1.5 * num * partsScanned / buf.size).toInt - partsScanned, 1)
       numPartsToTry =
         Math.min(numPartsToTry, partsScanned * scaleUpFactor)
     }
   }
```
 [spark@github](https://github.com/apache/spark/blob/master/core/src/main/scala/org/apache/spark/rdd/RDD.scala#L1332)



### Scala Implementations
* Scala - JVM
* ScalaJS
* Native Scala - LLVM Backend. Young project, not very mature yet.



### Beginners
* Which style should I follow?
* What are all these weird libraries and code?
* The compiler is too slow



### More advanced users
* Type safety
* Features combine in powerful ways
* Consise syntax
* Good integration with the host environment
* Expressive type system
* Very good execution speed
* The compiler is too slow*



### Object Oriented Programming
* Good methodology for structuring complex systems
* Define interfaces
* Communicate via message passing
* Appeared in the 70's



### Functional Programming
* Programming with mathematical functions
* Fewer errors
* Higher level abstractions
* Shorter code
* Increased productivity
* Mathematical reasoning



### Functional Programming Adoption Catalyzer

* Multicore architecture, no more faster CPUS
* Distributed computing (Internet and the Cloud)
* Immutable state makes things easier



### New Objects
* Eliminate or reduce mutable state
* Structural equality instead of reference equality
* Concentrate on the behaviour



### Can FP an OOP be combined?
![Can FP an OOP be combined?](../scaladays-2013-keynote-speech-by-martin-odersky-16-638.jpg?cb=1372789928)



### Scala and OOP



### Class hierarchy

![Scala class hierarchy](../class-hierarchy.png)



### Top types

* `Any`
	*  The base type of all types
	* Methods: `==, !=, equals, hashCode, toString`

* `AnyRef`
	* The base type of all reference types
	* Alias of `java.lang.Object`
* `AnyVal`
	* The base type of all primitive types



### Bottom types
* `Nothing` is at the bottom of Scala's type hierarchy. It is a subtype of every other type.
* There is no value of type `Nothing`.
  * To signal abnormal termination
  * As an element type of empty collections



### Classes
```scala
class Rational(x: Int, y: Int) {

  private def gcd(a: Int, b: Int): Int = 
    if (b == 0) a else gcd(b, a % b)

  private val g = gcd(x, y)

  def numer = x / g
  def denom = y / g

  def add(other: Rational): Rational = new Rational(
    numer * other.denom + other.numer * denom,
    denom * other.denom
  )
  override def toString = numer + "/" + denom
}

```



### Abstract Classes
```scala
abstract class IntSet {
  def add(x: Int): IntSet
  def contains(x: Int): Boolean
}
```



### Singleton Objects
```scala
object Empty extends IntSet {
  def contains(x: Int): Boolean = false
 ...
}
```



### Traits

A class can have only one superclass but can inherit code from many traits
```scala
trait Planar {
  def height: Int
  def width: Int
  def surface = height * width
}

class Square extends Shape with Planar with Movable …
```



### Polymorphism
```scala
abstract class Set[A] {
  def add(a: A): Set[A]
  def contains(a: A): Boolean
}

class Empty[A] extends Set[A] {
  …
}
```



### Generic Functions
```scala
def identity[A](x: A): A = x

identity[Int](1)
identity[Boolean](true)
```



### Local Type Inference
```scala
identity(1)
identity(true)
val x = 2 // x: Int
```



### Type Bounds
```scala
def selection[A <: Animal](a1: A, a2: A): A =
  if (a1.fitness > a2.fitness) a1 else a2

// Upper bound
A <: B means A is a subtype of B
In java: <A extends B> 

// Lower bound
A >: B means A is a supertype of B
In java: <A super B>

//Mixed bounds
A >: Zebra <: Animal
```



### Scala and Functional programming



### Everything is an expression
```scala
// Conditionals
val x = if (a > b) a else b

// blocks
{
  val a = 1
  a + 1
}
```



### Lexical Scoping
```scala
val x = 5
def f(y: Int) = x + y
val result = f(4)
// 9
```



### Tail Recursion
```scala
def length(l: List[_]): Int = {
  def inner(l: List[_], count: Int): Int =
    if (l.isEmpty) count
    else inner(l.tail, count + 1)

  inner(l, 0)
}

```



### Algebraic Data Types
```scala
trait Shape

case class Circle(radius: Double) extends Shape

case class Rectangle(width: Double, height: Double) extends Shape

```



### Case Classes
```scala
case class Rectangle(width: Double, height: Double)
```
* Immutable
* Equality definition
* Copy definition
* Pattern matching decomposition



### Pattern Matching
```scala
def area(s: Shape): Double =
  s match {
    case Circle(r) => 3.1415 * r * r
    case Rectangle(w, h) => w * h
  }
```



### Higher Order Functions
Attention!! Imperative example
```scala
def sum1to10: Int = {
  var accumulator = 0
  for (i <- 1 to 10) accumulator += i
  accumulator
}

//parametrise the data
def sumList(l: List[Int]): Int = {
  var accumulator = 0
  for (i <- l) accumulator += i
  accumulator
}
```


### Higher Order Functions (2)
```scala
// parametrise the initial value and the action
def foldList(initial: Int, action: Int => Int, l: List[Int]): Int = {
  var accumulator = initial
  for (i <- l) accumulator = action(accumulator, i)
  accumulator
}

def add(x: Int, y: Int): Int = x + y
def mul(x: Int, y: Int): Int = x * y

def sum(l: List[Int]): Int = foldList(0, add, l)
def prod(l: List[Int]): Int = foldList(1, mul, l)

```



### Currying
```scala
def foldList(
  initial: Int, 
  action: (Int, Int) => Int): (List[Int] => Int) = { l =>

   var accumulator = initial
   for (i <- l) accumulator = action(accumulator, i)
   accumulator
}

val fn = foldList(0, add) // res2: List[Int] => Int 
fn(List(1, 2, 3))

def foldList(initial: Int, action: (Int, Int) => Int)(l: List[Int]): Int = {
   var accumulator = initial
   for (i <- l) accumulator = action(accumulator, i)
   accumulator
}

```



### Partial application
```scala
def add(x: Int, y: Int): Int = x + y

val add4 = add(4, _: Int)
add4: Int => Int = $sess.cmd33$$$Lambda$2560/1548503636@4a3ed371

add4(4)
res0: Int = 8

```



### Call by name
```scala
def time[A](block: => A): A = {
   val t0 = System.nanoTime()
   val result = block
   val t1 = System.nanoTime()
   val elapsed = (t1 - t0) / (1000 * 1000 * 1000.0)
   println(s"Elapsed time: ${Math.round(elapsed * 100) / 100.0} seconds")
   result
}
```



### Implicits - Parameters

```scala
object Future {
  def apply[T](body: =>T)(implicit executor: ExecutionContext): Future[T] = ...
}

implicit val context = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

Future {
  println("I am running inside another thread")
}
```



### Advanced features
* Lazy values and Collections
* More Implicits (idioms and patterns)
* Dependent types
* Ad-hoc polymorphism (Typeclasses)
* Generic programming (HList)
* Higher Kinded Types
* F-Bounded polymorphism
* Structural (duck) typing
* Abstract type members
* and many more that I probably not know about.



### Advice
* Start small and add stuff as you need them
* Stick to the least powerful feature that can solve your problem
* [Strategic Scala Style: Principle of Least Power](http://www.lihaoyi.com/post/StrategicScalaStylePrincipleofLeastPower.html)
* [Scala with Style](https://www.youtube.com/watch?v=kkTFx3-duc8)



### Recommended reading
* [Programming in Scala](https://www.amazon.com/Programming-Scala-Comprehensive-Step-Step/dp/0981531644) by Martin Odersky*, Lex Spoon and Bill Venners
* [The Little Schemer](https://mitpress.mit.edu/books/little-schemer) if you are having troubles with recursion
* [Structure and Interpretation Of Computer Programs](https://mitpress.mit.edu/sicp/) one of the most appraised books on Programming 
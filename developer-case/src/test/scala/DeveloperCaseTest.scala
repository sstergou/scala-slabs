import java.time.LocalDateTime

import org.scalatest.{FlatSpec, Matchers}

import scala.io.Source
import scala.language.postfixOps

class DeveloperCaseTest extends FlatSpec with Matchers {
  val lines = Source.fromResource("tweets.json").getLines()
  val result = TrumpTweets.processTweets(lines.toIterable)

  "The result of processTweets" should "be the correct size" in {
    result.size shouldBe 24
  }

  it should "have users sorted chronologically" in {
    isSortedChronologically(result.map(_.user.created_at)) shouldBe true
  }

  it should "have tweets sorted chronologically" in {
    val allSorted = result.map(_.tweets).forall { tweets =>
      isSortedChronologically(tweets.map(_.created_at))
    }
    allSorted shouldBe true
  }

  def isSortedChronologically(it: Iterable[LocalDateTime]): Boolean =
    it.sliding(2).map(_.toVector).forall { v =>
      v.length < 2 || !v(0).isAfter(v(1))
    }
}

import trump.Domain.Formats._
import trump.Domain.{Tweet, UserTweets}
import trump.Orderings._
import trump.{Files, TwitterReader}
import trump.TimeUtil._
import spray.json._

import scala.collection.SortedMap
import scala.concurrent.duration._
import scala.language.postfixOps

object TrumpTweets extends App {
  val reader = new TwitterReader(
    "https://stream.twitter.com/1.1/statuses/filter.json?track=trump",
    "RLSrphihyR4G2UxvA0XBkLAdl",
    "FTz2KcP1y3pcLw0XXMX5Jy3GTobqUweITIFy4QefullmpPnKm4"
  )

  val rawTweets = time(reader.take(100, 10 seconds))

  println(s"Got ${rawTweets.length} tweets")

  val userTweets = processTweets(rawTweets)

  Files.write(userTweets.toJson.toString())
  terminate()

  def processTweets(rawTweets: Iterable[String]): Iterable[UserTweets] = {
    val parsed = rawTweets.toList.map(_.parseJson.convertTo[Tweet])
    val grouped = parsed.groupBy(_.user)
    val withSortedTweets = grouped.map { case (user, tweets) => user -> tweets.sortBy(_.created_at) }
    val withSortedUsers = SortedMap(withSortedTweets.toSeq: _*)
    withSortedUsers.map { case (user, tweets) => UserTweets(user, tweets) }
  }

  def terminate(): Unit = {
    reader.close()
    System.exit(0)
  }

}



package trump

import java.io.{File, FileOutputStream, IOException}

import grizzled.slf4j.Logging

object Files extends Logging {

  def write(text: String): Unit = {
    val temp: File = File.createTempFile("trump", ".json")
    val out = new FileOutputStream(temp)
    try {
      out.write((text + "\n").getBytes)
      info("Tweets saved at: " + temp.getAbsolutePath)
    } catch {
      case e: Exception => error("Error while writing file", e)
    }
  }
}

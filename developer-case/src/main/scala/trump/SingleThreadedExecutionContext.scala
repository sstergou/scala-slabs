package trump

import java.util.concurrent.Executors

import scala.concurrent.ExecutionContext

trait SingleThreadedExecutionContext {
  val executor = Executors.newSingleThreadExecutor()
  implicit val context = ExecutionContext.fromExecutor(executor)
}

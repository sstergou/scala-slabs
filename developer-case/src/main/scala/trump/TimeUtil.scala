package trump

import java.time.{LocalDateTime, ZoneOffset}

object TimeUtil {
  def toMillis(value: LocalDateTime): Long =
    value.toInstant(ZoneOffset.ofHours(0)).toEpochMilli

  def elapsed(d1: LocalDateTime, d2: LocalDateTime): Long =
    toMillis(d1) - toMillis(d2)

  def time[A](block: => A): A = {
      val t0 = System.nanoTime()
      val result = block
      val t1 = System.nanoTime()
      val elapsed = (t1 - t0) / (1000 * 1000 * 1000.0)
      println(s"Elapsed time: ${Math.round(elapsed * 100) / 100.0} seconds")
      result
    }
}

package trump

import java.io.{BufferedReader, InputStream, InputStreamReader}
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

import com.google.api.client.http.GenericUrl
import grizzled.slf4j.Logging
import oauth.twitter.TwitterAuthenticator
import trump.TimeUtil._

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps
import scala.util.{Success, Try}


class TwitterReader(url: String, key: String, secret: String)
  extends Logging
    with SingleThreadedExecutionContext {

  private val in: InputStream = {
    val auth = new TwitterAuthenticator(System.out, key, secret)
    val requestFactory = auth.getAuthorizedHttpRequestFactory
    val req = requestFactory.buildGetRequest(new GenericUrl(url))
    val resp = req.execute
    resp.getContent
  }

  private val reader = new BufferedReader(new InputStreamReader(in))

  def take(maxItems: Int, maxTime: Duration): Vector[String] = {
    val limit = LocalDateTime.now.plus(maxTime.toMillis, ChronoUnit.MILLIS)

    @scala.annotation.tailrec
    def recur(acc: Vector[String]): Vector[String] = {
      val now = LocalDateTime.now
      if (acc.length == maxItems || now.isAfter(limit)) acc
      else {
        val result = Try(Await.result(Future(Option(reader.readLine())), elapsed(limit, now) millis))
        result match {
          case Success(Some(str)) => recur(acc :+ str)
          case _ => acc
        }
      }
    }
    recur(Vector.empty[String])
  }

  def close(): Unit = {
    info("Closing connection...")
    in.close()
  }

}


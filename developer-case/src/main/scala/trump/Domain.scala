package trump

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import trump.TimeUtil._
import spray.json.{DefaultJsonProtocol, _}

import scala.util.{Failure, Success, Try}

object Domain {

  case class TwitterUser(id: Long, name: String, screen_name: String, created_at: LocalDateTime)

  case class Tweet(id: Long, text: String, user: TwitterUser, created_at: LocalDateTime)

  case class UserTweets(user: TwitterUser, tweets: List[Tweet])

  object Formats extends DefaultJsonProtocol {

    implicit object DateTimeFormat extends RootJsonFormat[LocalDateTime] {
      val TwitterTimeFormat = "EEE MMM dd HH:mm:ss Z yyyy"
      val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern(TwitterTimeFormat)

      def write(obj: LocalDateTime): JsValue = JsString(toMillis(obj).toString)

      def read(json: JsValue): LocalDateTime = json match {
        case JsString(s) =>
          Try(LocalDateTime.from(dateTimeFormatter.parse(s))) match {
            case Success(dt) => dt
            case Failure(ex) => error(json, ex)
          }
        case _ =>
          error(json, DeserializationException("Expected string json value"))
      }

      private def error(v: JsValue, ex: Throwable) =
        throw DeserializationException(s"Error while parsing ${v.toString} - ${ex.getMessage}")

    }

    implicit object UserTweetsFormat extends RootJsonFormat[UserTweets] {
      val baseFormat = jsonFormat2(UserTweets)
      override def read(json: JsValue): UserTweets = baseFormat.read(json)
      override def write(obj: UserTweets): JsValue = JsObject(
        "user" -> obj.user.toJson,
        "tweets" -> JsArray(obj.tweets.map { t =>
          JsObject(Map(
            "id" -> JsString(t.id.toString),
            "text" -> JsString(t.text),
            "created_at" -> DateTimeFormat.write(t.created_at)))
        }.toVector)
      )
    }


    implicit val twitterUserFormat: JsonFormat[TwitterUser] = jsonFormat4(TwitterUser)
    implicit val tweetFormat: JsonFormat[Tweet] = jsonFormat4(Tweet)
    implicit val userTweetsFormat: JsonFormat[UserTweets] = jsonFormat2(UserTweets)


  }

}

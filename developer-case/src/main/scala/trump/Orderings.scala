package trump

import java.time.LocalDateTime

import trump.Domain.TwitterUser

object Orderings {

  implicit val DateOrdering = new Ordering[LocalDateTime] {
    override def compare(x: LocalDateTime, y: LocalDateTime): Int = x.compareTo(y)
  }

  implicit val UserOrdering = new Ordering[TwitterUser] {
    override def compare(x: TwitterUser, y: TwitterUser): Int = x.created_at.compareTo(y.created_at)
  }

}

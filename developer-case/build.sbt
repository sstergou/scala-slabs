name := "developer-case"

version := "1.0"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  "com.google.oauth-client" % "google-oauth-client" % "1.22.0",
  "io.spray" %%  "spray-json" % "1.3.3",
  "org.clapper" %% "grizzled-slf4j" % "1.3.1",
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "org.slf4j" % "slf4j-simple" % "1.7.25",
  "org.slf4j" % "slf4j-api" % "1.7.25"
)

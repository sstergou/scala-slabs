The Expression Problem
----------------------

## Description

Imagine that we have a set of data types and a set of operations that act on these types.

Sometimes we need to add more operations and make sure they work properly on all types.

Sometimes we need to add more types and make sure all operations work properly on them. 

Sometimes, however, we need to add both - and herein lies the problem. 

Most of the mainstream programming languages don't provide good tools to add both new types and new operations to an existing system without having to change existing code. 

This is called the "expression problem". 

Studying the problem and its possible solutions gives great insight into the fundamental differences between object-oriented and functional programming and well as concepts like interfaces and multiple dispatch.

[source](http://eli.thegreenplace.net/2016/the-expression-problem-and-its-solutions/)

## Problem

Suppose you wish to describe shapes, including rectangles, and circles, and you wish to compute their areas.
1. Write a program in your favourite language (or scala) that solves this problem.
2. Imagine that your program is now a geometry library. A colleague of yours uses another geometry library that provides similar functionality but for other shapes such as trapezoids, ellipses etc. The other library looks like that:

```
interface Planar {
    double surface();
}

class Ellipse implements Planar {
    public double surface() {
        ...
    }
}

class Trapezoid implements Planar {
   public double surface() {
      ...
   }
}

```

Can you write a function that computes the area of an object that might come from any of the two libraries?

Can you easily extend you solution to possibly include other types?
What if you need to add a circumference operation to all the types?

*For the sake of understanding the previous problem, the reader must try to implement each step independently. This is usually the case as in real life programmers try to interact with libraries that they have no control over.*
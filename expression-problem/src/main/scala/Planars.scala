object Planars {

  trait Planar {
    def surface: Double
  }

  case class Trapezoid(a: Double, b: Double, h: Double) extends Planar {
    override def surface: Double = a * b * h
  }

  case class Ellipse(r1: Double, r2: Double) extends Planar {
    override def surface: Double = Math.PI * r1 * r2
  }

}

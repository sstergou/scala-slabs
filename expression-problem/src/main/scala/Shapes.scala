object Shapes {

  trait Shape {
    def area: Double
  }

  case class Rectangle(width: Double, height: Double) extends Shape {
    override def area: Double = width * height
  }

  case class Circle(radius: Double) extends Shape {
    override def area: Double = Math.PI * radius * radius
  }

}

import Shapes._
import Planars._

object ExpressionProblem extends App {


  trait Geom[-A] {
    def space(a: A): Double
  }

  implicit val ShapeIsGeom = new Geom[Shape] {
    override def space(s: Shape): Double = s.area
  }

  implicit val PlanarIsGeom = new Geom[Planar] {
    override def space(a: Planar): Double = a.surface
  }

  def computeArea[A](a: A)(implicit g: Geom[A]): Double =
    g.space(a)


  val c = Circle(1)
  println(computeArea(c))

  val e = Ellipse(3, 4)
  println(computeArea(e))


}
